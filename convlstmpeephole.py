import code
import sys

import numpy
import chainer
import chainer.functions as F
from chainer.functions import batch_normalization as BN
import chainer.links as L

import lstmpeephole

class ConvLSTMPeephole(chainer.Chain):
    
    def __init__(self, in_channels, out_channels, height,  width, wscale=1, nobias=False, initialW=None, initial_bias=None):
        super(ConvLSTMPeephole, self).__init__(
            # upward connections
            x_i=L.Convolution2D( in_channels, out_channels, 3, pad=1),
            x_f=L.Convolution2D( in_channels, out_channels, 3, pad=1),
            x_c=L.Convolution2D( in_channels, out_channels, 3, pad=1),
            x_o=L.Convolution2D( in_channels, out_channels, 3, pad=1),
            # lateral connections
            h_i=L.Convolution2D(out_channels, out_channels, 3, pad=1),
            h_f=L.Convolution2D(out_channels, out_channels, 3, pad=1),
            h_c=L.Convolution2D(out_channels, out_channels, 3, pad=1),
            h_o=L.Convolution2D(out_channels, out_channels, 3, pad=1)
        )
        
        self.out_channels = out_channels
        self.out_size = out_channels * height * width
        self.add_param('W', (self.out_size*3,1))
        if initialW is None:
            initialW = numpy.random.normal(
                0, wscale * numpy.sqrt(1. / (self.out_size*3)), (self.out_size*3, 1))
        self.W.data[...] = initialW

        # batch normalization variables
        self.add_param('gammax',None)
        self.add_param('betax',None)
        self.add_param('gammah',None)
        self.add_param('betah',None)
        self.gammax.data[...] = self.xp.array(0.1)
        self.betax.data[...]  = self.xp.array(0.0)
        self.gammah.data[...] = self.xp.array(0.1)
        self.betah.data[...]  = self.xp.array(0.0) 
        print(0)
        print('TESTING')

        
        if nobias:
            self.b = None
        else:
            self.add_param('b', (1, self.out_size*3))
            #if initial_bias is None:
            #    initial_bias = bias
            self.b.data[...] = initial_bias
        
        
        self.reset_state()
        
    def reset_state(self):
        
        self.c = self.h = self.cs = None
            
    def __call__(self, x):
        batchsize, in_channels, height, width = x.data.shape
        # convolution of external inputs to input-gate, forget-gate, cell, and output-gate
        xi_conv = self.x_i(x)   # shape: (batchsize, output_channels, w, h)
        xf_conv = self.x_f(x)
        xc_conv = self.x_c(x)
        xo_conv = self.x_o(x)
        # concatenate convolutioned external inputs to input-gate, forget-gate, cell, and output-gate
        xis = F.reshape(xi_conv, (batchsize, self.out_channels, 1, height * width)) # a, a.shape = (4,61440), xis.shape = (4,192,1,320)
        xfs = F.reshape(xf_conv, (batchsize, self.out_channels, 1, height * width)) # i
        xcs = F.reshape(xc_conv, (batchsize, self.out_channels, 1, height * width)) # f
        xos = F.reshape(xo_conv, (batchsize, self.out_channels, 1, height * width)) # o

        if self.h is not None:
            hi_conv = self.h_i(self.h)
            hf_conv = self.h_f(self.h)
            hc_conv = self.h_c(self.h)
            ho_conv = self.h_o(self.h)

            his = F.reshape(hi_conv, (batchsize, self.out_channels, 1, height * width))
            hfs = F.reshape(hf_conv, (batchsize, self.out_channels, 1, height * width))
            hcs = F.reshape(hc_conv, (batchsize, self.out_channels, 1, height * width))
            hos = F.reshape(ho_conv, (batchsize, self.out_channels, 1, height * width))

            xis += his
            xfs += hfs
            xcs += hcs
            xos += hos

        if self.h is None:
            self.h = chainer.Variable(
                            self.xp.zeros((batchsize, self.out_channels * height * width), dtype=x.data.dtype),
                            volatile='auto')
        if self.b is None:
            self.b = chainer.Variable(
                            self.xp.zeros((1, self.out_channels * height * width * 3), dtype=x.data.dtype),
                            volatile='auto')
        if self.cs is None:
            self.cs = chainer.Variable(
                            self.xp.zeros((batchsize, self.out_channels * height * width), dtype=x.data.dtype),
                            volatile='auto')
        print(1)
        # LSTM W/ BN activations
        # is it *self.cs or is it self.h, given that we are doing lstm instead of lstmpeephole?
        w_idx = self.W.shape[0] // 3
        print(2)
        b_idx = self.b.shape[1] // 3
        print(3)
        self.a = F.tanh(xis)
        print(4)
        self.i = F.sigmoid(  BN(xfs, self.gammax, self.betax) 
                      + BN(self.W[ :w_idx] * self.cs, self.gammah, self.betah)
                      + self.b[ : b_idx])
        print(5)
        self.f =  F.sigmoid(  BN(xcs, self.gammax, self.betax)
                       + BN(self.W[w_idx : w_idx*2] * self.cs, self.gammah, self.betah)
                       + self.b[b_idx : b_idx*2])
        print(6)
        self.c = (a * i) + (f * self.cs)
        print(7)

        self.o = F.sigmoid(  BN(xos, self.gammax, self.betax)
                      + BN(self.W[w_idx*2 : ] * self.cs, self.gammah, self.betah)
                      + self.b[b_idx*2 : ])
        print(8)
        self.h = o * F.tanh(BN(c, gammac, betac))
        print(9)
        self.cs = self.c
        print(10)

        #lstm_ins  = F.reshape(F.concat((xis, xfs, xcs, xos), axis=2), (batchsize, self.out_channels * 4 * height * width))



                
        
        
        
        #lstm_ins.shape = (4, 245760), on first run at least



        #self.cs, hs = F.lstm(self.cs, lstm_ins)
        # LSTM w/ peephole update
        #self.cs, hs = lstmpeephole.lstmpeephole(self.cs, lstm_ins, self.W) #, self.b)
            
        # reshape back into the "image-wise" format
        #self.c = F.reshape(self.cs, (batchsize, self.out_channels, height, width))
        #self.h = F.reshape(hs, (batchsize, self.out_channels, height, width))
        code.interact(local=dict(globals(), **locals()))
        sys.exit()
        return self.h
            
                    
            
            
            
            
        
        


"""
import numpy
import chainer
import chainer.functions as F
import chainer.links as L

import lstmpeephole

class ConvLSTMPeephole(chainer.Chain):
    
    def __init__(self, in_channels, out_channels, height,  width, wscale=1, nobias=False, initialW=None, initial_bias=None):
        super(ConvLSTMPeephole, self).__init__(
            # upward connections
            x_i=L.Convolution2D( in_channels, out_channels, 3, pad=1),
            x_f=L.Convolution2D( in_channels, out_channels, 3, pad=1),
            x_c=L.Convolution2D( in_channels, out_channels, 3, pad=1),
            x_o=L.Convolution2D( in_channels, out_channels, 3, pad=1),
            # lateral connections
            h_i=L.Convolution2D(out_channels, out_channels, 3, pad=1),
            h_f=L.Convolution2D(out_channels, out_channels, 3, pad=1),
            h_c=L.Convolution2D(out_channels, out_channels, 3, pad=1),
            h_o=L.Convolution2D(out_channels, out_channels, 3, pad=1)
        )
        
        self.out_channels = out_channels
        self.out_size = out_channels * height * width
        self.add_param('W', (self.out_size*3,1))
        if initialW is None:
            initialW = numpy.random.normal(
                0, wscale * numpy.sqrt(1. / (self.out_size*3)), (self.out_size*3, 1))
        self.W.data[...] = initialW
        
        if nobias:
            self.b = None
        else:
            self.add_param('b', (1, self.out_size*3))
            #if initial_bias is None:
            #    initial_bias = bias
            self.b.data[...] = initial_bias
        
        
        self.reset_state()
        
    def reset_state(self):
        
        self.c = self.h = self.cs = None
            
    def __call__(self, x):
        batchsize, in_channels, height, width = x.data.shape
        # convolution of external inputs to input-gate, forget-gate, cell, and output-gate
        xi_conv = self.x_i(x)   # shape: (batchsize, output_channels, w, h)
        xf_conv = self.x_f(x)
        xc_conv = self.x_c(x)
        xo_conv = self.x_o(x)
        # concatenate convolutioned external inputs to input-gate, forget-gate, cell, and output-gate
        xis = F.reshape(xi_conv, (batchsize, self.out_channels, 1, height * width))
        xfs = F.reshape(xf_conv, (batchsize, self.out_channels, 1, height * width))
        xcs = F.reshape(xc_conv, (batchsize, self.out_channels, 1, height * width))
        xos = F.reshape(xo_conv, (batchsize, self.out_channels, 1, height * width))
        lstm_ins  = F.reshape(F.concat((xis, xfs, xcs, xos), axis=2), (batchsize, self.out_channels * 4 * height * width))
        
        if self.h is not None:
            # convolution of recurrent inputs to input-gate, forget-gate, cell, and output-gate
            hi_conv = self.h_i(self.h)
            hf_conv = self.h_f(self.h)
            hc_conv = self.h_c(self.h)
            ho_conv = self.h_o(self.h)
            # concatenate convolutioned recurrent inputs to input-gate, forget-gate, cell, and output-gate
            his = F.reshape(hi_conv, (batchsize, self.out_channels, 1, height * width))
            hfs = F.reshape(hf_conv, (batchsize, self.out_channels, 1, height * width))
            hcs = F.reshape(hc_conv, (batchsize, self.out_channels, 1, height * width))
            hos = F.reshape(ho_conv, (batchsize, self.out_channels, 1, height * width))
            lstm_ins += F.reshape(F.concat((his, hfs, hcs, hos), axis=2), (batchsize, self.out_channels * 4 * height * width))
                
        if self.cs is None:
            self.cs = chainer.Variable(
                            self.xp.zeros((batchsize, self.out_channels * height * width), dtype=x.data.dtype),
                            volatile='auto')
        
        # LSTM w/ peephole update
        self.cs, hs = lstmpeephole.lstmpeephole(self.cs, lstm_ins, self.W) #, self.b)
            
        # reshape back into the "image-wise" format
        #self.c = F.reshape(self.cs, (batchsize, self.out_channels, height, width))
        self.h = F.reshape(hs, (batchsize, self.out_channels, height, width))
        return self.h
"""
                    
            
            
            
            
        
        