# Where KITTI data will be saved if you run process_kitti.py
# If you directly download the processed data, change to the path of the data.
DATA_DIR = '/home/evan/samba/data/npy/'

# Where model weights and config will be saved if you run kitti_train.py
# If you directly download the trained weights, change to appropriate path.
MODEL_DIR = './Model/'

# Where results (prediction plots and evaluation file) will be saved.
RESULTS_SAVE_DIR = './Evaluation_Results/'
